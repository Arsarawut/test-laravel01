<header>
    <nav class="navbar navbar-expand-sm <!--bg-light--!> navbar-light">
        <a class="navbar-brand" href="#">LOGO</a>
        <div class="tel">+6698 765 4321</div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="#">หน้าแรก</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">ผลิตภัณฑ์</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">เช็คเบี้ยประกัน</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">ซื้อประกันออนไลน์</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">ตัวแทนประกัน</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">ติดต่อเรา</a>
            </li>
            <li class="nav-item navmenu-user">
              <a class="nav-link" href="#"> <i class="fa fa-user-o" aria-hidden="true"></i>  Username     <i class="fa fa-angle-down" aria-hidden="true"></i></a>
            </li>
          </ul>
        </div>
      </nav>
</header>
