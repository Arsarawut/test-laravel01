<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
{{-- start css --}}
<link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{asset("css/app.css")}}">
@yield('style')
{{-- end css --}}
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <script src="{{asset("js/app.js")}}"></script>
</head>

<style>
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('{{asset('img/loading/loader-64x/Preloader_2.gif')}}') center no-repeat #fff;
    }
</style>

<body>
    <div class="se-pre-con"></div>
    @include('template/header')

    @yield('content')

    @include('template/footer')



    <script type="text/javascript">
        //paste this code under the head tag or in a separate js file.
         // Wait for window load
        $(window).on('load', function(){
            //alert("test");
            // Animate loader off screen
            $(".se-pre-con").fadeOut("slow");
        });
    </script>

    @yield('script')
</body>
</html>
