@extends('template/all')
@section('content')

<div class="container" style="margin-top:30px">
    <div class="row">
      <div class="col-sm-4">
            <div class="box-left">
                <h3>รายการประกันภัยของคุณ</h3>
                <h4>ทิพยประกันภัย</h4>
                <h4>ทิพยจัดเต็ม</h4>

                <p>วันที่เริ่มความคุ้มครอง 1/11/2019</p>
                <p>วันที่สิ้นสุดความคุ้มครอง 1/11/2019</p>
            </div>

            <div class="box-left box-left2">
                <h3>ข้อมูลผู้รับความุ้มครอง</h3>
                <h4>สมหมาย ตระกูลใหญ่</h4>

                <p>11/12 บ้านโคก ตำบลหนองจอก อำเภอมิ่งใหญ่ <br><br><br> จังหวัดกรุงเทพมหานคร 10210</p>
            </div>
      </div>
      <div class="col-sm-8">


        <div class="content-payment">
            <h2>สรุปรายการฃำระเงิน</h2>

            <div class="row">
                <div class="col-6 col-md-6"><span>ทิพยประกันภัยทิพยจัดเต็ม</span></div>
                <div class="col-6 col-md-6 text-right"><span>3,050 บาท</span></div>
            </div>

            <div class="row row-sum">
                <div class="col-6 col-md-6"><span>ยอดเงินรวม</span></div>
                <div class="col-6 col-md-6 text-right"><span>3,050 บาท</span></div>
            </div>


        </div>

        <div class="checkbox-allow">
            <label class="container">ฉันยอมรับเงื่อนไขและข้อตกลง
                <input type="checkbox" checked="checked">
                <span class="checkmark"></span>
              </label>
        </div>


        <div id="payment-way">
            <div class="row no-gutters">
                <div class="head-payment-way"><h2>เลือกวิธีการชำระเงิน</h2></div><br>
                <div class="col-sm-12 col-md-6">
                    <div class="card card1">
                        {{-- <div class="card-header">Header</div> --}}
                        <div class="card-body">
                            <h2>บัตรเครดิต</h2>
                        <div class="icon-credit"><i class="fa fa-id-card" aria-hidden="true"></i></div>

                        <span>เรายินดีรับบัครเครดิตทุกธนาคาร</span><br>

                        <div class="btn-pay">ชำระเงิน</div>

                        <div class="logo-credit">
                            <img src="{{asset('img/visa.png')}}">
                            <img src="{{asset('img/_mastercard-512.png')}}">
                            <img src="{{asset('img/download.png')}}">
                        </div>

                        </div>
                        {{-- <div class="card-footer">Footer</div> --}}



                    </div>
                </div>


                <div class="col-sm-12 col-md-6">
                    <div class="card card2">
                        {{-- <div class="card-header">Header</div> --}}
                        <div class="card-body">
                            <h2>โอนผ่านธนาคาร</h2>
                        <div class="icon-credit"><i class="fa fa-file-text-o" aria-hidden="true"></i></div>

                        <span>จะโอนผ่าน mobile banking หรือ<br>โอนผ่านตู้เอทีเอ็ม เพียงแค่แจ้งสลิป</span><br>

                        <div class="btn-pay btn-pay2">ชำระเงิน</div>

                        <div class="logo-credit">
                            <img src="{{asset('img/visa.png')}}">
                            <img src="{{asset('img/_mastercard-512.png')}}">
                            <img src="{{asset('img/download.png')}}">
                            <img src="{{asset('img/visa.png')}}">
                            <img src="{{asset('img/_mastercard-512.png')}}">
                            <img src="{{asset('img/download.png')}}">
                            <img src="{{asset('img/download.png')}}">
                        </div>

                        </div>
                        {{-- <div class="card-footer">Footer</div> --}}



                    </div>
                </div>


            </div>
        </div>



      </div>
    </div>
  </div>


@endsection
